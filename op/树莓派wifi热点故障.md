## 现象
树莓派在某次断电重启之后，热点信号就再没有出现过。由于之前配置wifi的时候没有做记录，导致本次无从下手。

## 找到配置方式
通过搜索，得知一种用来发射wifi的方式叫做`hostapd`。根据其配置文件的默认路径，找到了相应配置，从而可以确认我之前所使用的应该是这个软件。
## 调试

``` bash
systemctl status hostapd
```
``` text
● hostapd.service - Advanced IEEE 802.11 AP and IEEE 802.1X/WPA/WPA2/EAP Authenticator
   Loaded: loaded (/lib/systemd/system/hostapd.service; enabled; vendor preset: enabled)
   Active: inactive (dead) since Fri 2021-03-12 09:08:31 CST; 15s ago
  Process: 14330 ExecStart=/usr/sbin/hostapd -B -P /run/hostapd.pid -B $DAEMON_OPTS ${DAEMON_CONF} (code=exited, status=0
 Main PID: 14340 (code=exited, status=0/SUCCESS)

Mar 12 09:08:24 raspbian systemd[1]: Starting Advanced IEEE 802.11 AP and IEEE 802.1X/WPA/WPA2/EAP Authenticator...
Mar 12 09:08:24 raspbian hostapd[14330]: Configuration file: /etc/hostapd/hostapd.conf
Mar 12 09:08:24 raspbian hostapd[14330]: wlan0: interface state UNINITIALIZED->COUNTRY_UPDATE
Mar 12 09:08:24 raspbian systemd[1]: Started Advanced IEEE 802.11 AP and IEEE 802.1X/WPA/WPA2/EAP Authenticator.
Mar 12 09:08:31 raspbian systemd[1]: hostapd.service: Succeeded.
```
查看hostapd日志，发现 该服务已经关闭，出现异常代码`UNINITIALIZED->COUNTRY_UPDATE`

systemd 服务的日志不全，可以使用下面的命令启动服务：

```bash
/usr/sbin/hostapd /etc/hostapd/hostapd.conf
```
启动日志
``` text
Configuration file: /etc/hostapd/hostapd.conf
wlan0: interface state UNINITIALIZED->COUNTRY_UPDATE
wlan0: interface state COUNTRY_UPDATE->HT_SCAN
Failed to request a scan of neighboring BSSes ret=-52 (Invalid exchange)
wlan0: interface state HT_SCAN->DISABLED
wlan0: AP-DISABLED 
wlan0: interface state DISABLED->DISABLED
wlan0: AP-DISABLED 
wlan0: CTRL-EVENT-TERMINATING 
hostapd_free_hapd_data: Interface wlan0 wasn't started
nl80211: deinit ifname=wlan0 disabled_11b_rates=0
```
启动失败，看日志可能是wifi 模块没有启动。佐证：

``` bash
ip link | grep wlan0
```

``` text
wlan0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq state DOWN mode DEFAULT group default qlen 1000
```
尝试了启动wlan0模块的命令，似乎都没有办法启动。难道是wifi模块坏了？可以通过内核启动日志进一步分析。

``` bash
dmesg
```
国家代码以及可选信道
https://zh.wikipedia.org/wiki/WLAN%E4%BF%A1%E9%81%93%E5%88%97%E8%A1%A8#5_GHz_(802.11a/h/j/n/ac/ax)[11]