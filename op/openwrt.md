# 宿主机

## nano /etc/network/interfaces.d/op

```
# 利用树莓派自带的千兆网口eth0和usb网口eth1实现软路由
# 创建桥接子网
# 主要用于绑定物理网口eth1（usb网卡）和wlan0（无线网卡）
auto br-lan
iface br-lan inet static
    address 192.168.3.254
    netmask 255.255.255.0
    gateway 192.168.3.253
    dns-nameservers 192.168.3.252
    bridge_ports eth1 wlan0
    # 修改宿主机默认路由，流量经过openwrt转发
    # https://github.com/lisaac/blog/issues/4
    # https://manpages.debian.org/testing/ifupdown/interfaces.5.en.html
    post-up ip route add default via 192.168.3.253 dev br-lan
    post-down ip route del default

# usb网卡支持热插拔
allow-hotplug eth1
iface eth1 inet manual
    up ip link set eth1 promisc on

# eth0和上面的eth1设置支持混合模式
iface eth0 inet manual
    up ip link set eth0 promisc on

```

重启宿主机网络

```bash
service networking reload
```

## 创建 macvlan ，并且绑定上 eth0 网口

```
docker network create -d macvlan -o parent=eth0 wanet
```

## 创建桥接模式的子网，用于 op 容器以及其他需要联网的容器

```
docker network create -d bridge \
  --subnet=192.168.3.0/24 --gateway=192.168.3.254 \
  -o "com.docker.network.bridge.name"="br-lan" \
  lanet
```

## op 容器准备

```
docker import /path/to/op-container.gz op-rpi:latest
```

创建容器，绑定 ip

> 需要固定 ip 的容器，从大往下定义，这样子在后面需要增加到 lanet 网络中的容器中，就不会出现与之前定义好的 ip 出现重复的情况

TODO: 需要补一张拓扑图

```
docker run -d --restart unless-stopped --name op \
  --network lanet --privileged \
  --ip 192.168.3.253 \     
  op-rpi /sbin/init
```

```
docker network connect wanet op
```

```
docker inspect op
```

# 容器

## vi /etc/config/network

```
config interface 'lan'
        option type 'bridge'
        option ifname 'eth0'
        option proto 'static'
        option ipaddr '192.168.3.253'
        option netmask '255.255.255.0'
        option ip6assign '60'
        option dns '192.168.3.252'

config interface 'wan'
        option ifname 'eth1'
        option _orig_ifname 'eth1'
        option _orig_bridge 'false'
        option proto 'dhcp'
        option hostname 'ruanxihaodeMiniPc'

config interface 'PPPOE'
        option proto 'pppoe'
        option username '210081943929'
        option password '943929'
        option ipv6 'auto'
        option keepalive '0'
        option _orig_ifname 'eth0'
        option _orig_bridge 'false'
        option ifname 'eth1'
        option auto '0'
```

重启网络

```bash
/etc/init.d/network restart
```

## 宿主机中创建更多子网

> 2020.09.21 方案不是非常可行

http://jodies.de/ipcalc?host=192.168.3.193&mask1=26&mask2=

htpasswd -B -n -b ad ad@5768