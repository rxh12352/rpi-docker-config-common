### 网络

公共服务使用了 lanet 和 nginxnet。其中，`lanet`与 openwrtl 的 lan 网络桥接，使用了`lanet`的容器也就等同于链接上了 openwrt 的局域网。`nginxnet`则是专门为 web 服务创建的网络，与该网络链接的容器之间都可以通过 docker-compose 中配置的容器名字便捷访问。

> PS: 通过容器名字便捷访问容器的这个配置，需要特定的 docker-compose 配置文件的版本

常见的网络命令有：

```bash
# 列出所有网络
docker network ls

# 查看网络中的容器以及其他配置
docker network inspect nginxnet
```

更多 docker 网络相关的命令，可以查阅官方文档。

### 配置模版

- mysql

```yaml
mysql:
  image: mysql:5.6
  networks:
    - base_network
  environment:
    TZ: Asia/Shanghai
    # 这个配置没啥用，密码一直是默认的root
    MYSQL_ROOT_PASSWORD: "123456"
  ports:
    - "3306:3306"
  volumes:
    - mysql:/var/lib/mysql
    - ./mysql/custom.cnf:/etc/mysql/conf.d/custom.cnf
```

- nginx

```yaml
nginx:
  image: nginx:latest
  restart: unless-stopped
  environment:
    TZ: Asia/Shanghai
  networks:
    - nginxnet
  volumes:
    - ./nginx/www:/www
    - ./nginx/default.conf:/etc/nginx/nginx.conf
    - ./nginx/conf:/etc/nginx/_conf
    - ./nginx/logs:/var/logs
    - ./certbot/conf:/etc/letsencrypt
    - ./certbot/www:/var/www/certbot
  ports:
    - "80:80"
    - "443:443"
  command: '/bin/sh -c ''while :; do sleep 6h & wait $${!}; nginx -s reload; done & nginx -g "daemon off;"'''
```

- samba

```yaml
samba:
  image: dperson/samba:aarch64
  restart: unless-stopped
  # environment:
  #   - PUID=1000
  #   - PGID=1000
  ports:
    - "139:139"
    - "445:445"
  volumes:
    - "/root/downloads:/mount/downloads"
    - "/media/ssd:/mount/usb"
  command: "-p -u 'root;raspberry' -s 'raspberry;/mount;yes;no;no;root;root;root'"
```
