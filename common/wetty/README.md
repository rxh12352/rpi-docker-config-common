## webtty

使用 web 的方式访问远程主机。被访问的机器也可以用作跳板机，继续访问网络中的其他机器。

## docker-compose 配置以及常用指令

- 配置文件

```yaml
wetty:
  build:
    context: ./wetty
    network: host
  restart: unless-stopped
  networks:
    nginxnet:
    lanet:
      ipv4_address: 192.168.3.252
  volumes:
    - ./wetty/ssh-config:/home/node/.ssh
```

- docker-compose up -d --force --build wetty

由于采用 Dockerfile 的形式作为镜像的来源，因此每当 Dockerfile 修改的时候，都需要使用`--build`命令重新构建镜像。
