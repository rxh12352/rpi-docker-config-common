## 说明

openresty 可以用来替代 nginx ，并且迁移的时候，原来的 nginx 配置可以完美兼容。在这个项目里面，只需要将配置放在`conf.d`目录中即可。默认配置（即`default.conf`）已经由容器自己创建了，如果没有特殊需要，可以不用关心这个文件。

## 常用的 location 写法

- adguard home

```text

location /ad/ {
    more_set_headers 'Location: /login.html' 'Location: /ad/login.html';
    proxy_set_header Host $host;
    proxy_pass http://adguard_home/;
}

location /control {
    proxy_pass http://adguard_home/;

}
```

实现的效果：

http://server/ad/ -> http://adguard_home/
http://server/control -> http://adguard_home/control
