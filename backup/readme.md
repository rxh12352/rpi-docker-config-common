# 系统文件备份

1. op 容器

openwrt容器包含配置信息（运行的时候不能使用-v映射），备份命令：

```
docker export op | gzip > op-container.gz
```

2. 配置文件

使用tar的功能，打包`*.lst`文件，实现全部配置文件的备份。

# 使用方式

进入`/path/to/docker-config/backup`，执行`sh ./run.sh`即可。